FROM python:3.6-alpine

LABEL maintainer="imil@esunix.com"

RUN adduser --system flask

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY myhead.py .

ENV FLASK_APP myhead.py
ENV REDIS_HOST redis

RUN chown -R flask .
USER flask

EXPOSE 5000
CMD ["python", "/usr/local/bin/flask", "run", "--host=0.0.0.0"]
