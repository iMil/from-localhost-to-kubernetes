#!/usr/bin/env python

from flask import Flask, request, jsonify
import redis
import os
import socket

app = Flask(__name__)

redis_host = os.environ["REDIS_HOST"]

r = redis.Redis(host=redis_host)

@app.route('/')
def whoisit():
    host = request.remote_addr  # laziness :)

    if not r.get(host):
        r.set(host, 1)
    else:
        r.set(host, int(r.get(host)) + 1)
    return jsonify({
        'remote_addr': host,
        'my_addr': socket.gethostbyname(socket.gethostname()),
        'seen': r.get(host).decode()  # in python 3, binary returned from get()
    })
